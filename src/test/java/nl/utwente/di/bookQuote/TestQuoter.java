package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestQuoter {
    @Test
    public void testTemperature() throws Exception{
        Quoter quoter = new Quoter();
        double price = quoter.getBookPrice("20.5");
        Assertions.assertEquals(68.9, price, 0.0, "temperature in Fahrnheit");
    }
}
