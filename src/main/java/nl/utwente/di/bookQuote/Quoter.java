package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double getBookPrice(String temperature){
        double result = ((Double.parseDouble(temperature) * 9/5) + 32);
        return result;
    }
}
